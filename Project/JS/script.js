// import * as Sceleton from './aplications/sceleton';
// import * as DomElement from './aplications/section';

const top_page = 'top_page';
const lorem = `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`;
const scelet = 'scelet';

const start = () => {

    let skel = JSON.parse( localStorage.getItem(scelet) );
    if (skel) {
        let obj = new Sceleton(scelet);
        skel = Object.assign(obj, skel);
    } else {
        skel = new Sceleton();
        let sec1 = new DomElement('item1', 'Your first post', '', '', lorem, top_page);
        sec1.store();
        skel.navbar.push(sec1.id);
    }
    skel.render();

}

window.addEventListener('load', start);
document.addEventListener('selectionchange', start);





class DomElement {
    constructor(menuItem, title, background, img, inner, id='') {
        this.id = (id) ? id : new Date().getTime();
        this.menuItem = menuItem;
        this.title = title;
        this.background = background;
        this.img = img;
        this.inner = inner;
    };
    render() {

        const hoverSect = () => {
            let butns = domElement.querySelector('.editable');
            butns.classList.add('show');
        }
        const unhovSect = () => {
            let butns = domElement.querySelector('.editable');
            butns.classList.remove('show');
        }
        
        const mainMenu = document.getElementById('menu');
        let menuItem = document.createElement('a');
        menuItem.href = `#${this.id}`;
        menuItem.className = "item";
        menuItem.dataset.id = this.id;
        menuItem.innerText = this.menuItem;
        mainMenu.appendChild( menuItem );
        let domElement = document.createElement('section');
        domElement.dataset.id = this.id;
        domElement.className = "main-menu nav-container section";
        domElement.style.backgroundImage = this.background
        domElement.innerHTML = `
            <div class="editable">
                <button class="edit" onclick="addsection(${this.id})">Edit</button>
                <button class="delete" onclick="removesect(${this.id})">Delete</button>
            </div>
            <a name="${this.id}"><h3 class="title">${this.title}</h3></a>
            <img src="${this.img}">
            <div class="content">
                ${this.inner}
            </div>`;
        domElement.addEventListener('mouseover', hoverSect);
        domElement.addEventListener('mouseleave', unhovSect);
        const body = document.getElementById('body');
        body.appendChild( domElement );

        
    };
    store() {
        localStorage.setItem(this.id, JSON.stringify(this) );
    };
    fetch(name) {
        const element = JSON.parse( localStorage.getItem(name) );
        for (key in this) {
            this.key = element.key;
        }
    };
    remove() {
        localStorage.removeItem(this.id);
    };
    set(menuItem='', title="", background="", img="", inner="") {
        this.menuItem = (menuItem) ? menuItem : this.menuItem;
        this.title = (title) ? title : this.title;
        this.background = (background) ? `url(${background})` : this.background;
        this.img = (img) ? `url(${img})` : this.img;
        this.inner = (inner) ? inner : this.inner;
        return this;
    };
};

class Sceleton {
    constructor(name=scelet) {
        this.name = name;
        this.style = {bg: "#00001a", color: "#fff"};
        this.brand = './images/vimeo-letter-logo-in-a-square.png';
        this.title = "My Portfolio Page"
        this.navbar = [];
        this.sidebar = null;
        this.footer = "<p>Created with exciting</p>";
    };
    
    render() {
        document.getElementById('brand').innerHTML = `<img src="${this.brand}" 
                        alt="brand" width="64" height="64"></img>`;
        document.getElementById('title').innerText = this.title;
        document.body.style.backgroundColor = this.style.bg;
        document.body.style.color = this.style.color;
        document.body.children.footer.innerHTML = this.footer;
        document.getElementById('body').innerHTML = null;
        document.getElementById('menu').innerHTML = null;
        this.navbar.map(item => {let it = fetch(item);
                                        it.render()});
        const header = document.querySelector('header');
        const edit = document.createElement('div');
        edit.className = "editable";
        edit.innerHTML = `<button class="edit" onclick="editMenu('${this.name}')">Edit menu</button>
                          <button class="edit" onclick="editThem('${this.name}')">Edit theme</button>`;
        header.appendChild(edit);
        header.addEventListener('mouseover', () => {
            edit.classList.add('show');
        });
        header.addEventListener('mouseleave', () => {
            edit.classList.remove('show');
        });
        this.store();
    };
    store() {
        localStorage.setItem(this.name, JSON.stringify(this) );
    };
    fetch() {
        const element = JSON.parse( localStorage.getItem(this.name) );
        if (element) {
            Object.assign(this, element)
        }
    };
    remove() {
        localStorage.removeItem(this.name);
    };
};



const addsection = (num) => {
    const item = fetch(num);
    const popupdiv = document.querySelector('.popup');
    const popup = `
    <div class="popup__body">
        <button type='submit' for="form" class="popup__save" id="popup__add" onclick='savechanges(${num})'>Save</button>
        <button type='submit' for="form" class="popup__close" id="popup__close" onclick='popupclose()'>Close</button>
        <form id="myForm" class="popup__container" id="popup__container">
            <label class="main-menu">
                <span>Menu item</span>
                <input type="text" name="item" placeholder="${item.menuItem}">
            </label>
            <label class="main-menu">
                <span>Title</span>
                <input type="text" name="title" placeholder="${item.title}">
            </label>
            <label class="main-menu">
                <span>Background image source</span>
                <input type="url" name="background">
            </label>
            <label class="main-menu">
                <span>Image source</span>
                <input type="url" name="image">
            </label>
            <label class="main-menu">
                <span>Write text content or inner HTML</span>
                <textarea>${item.inner}</textarea>
            </label>
        </form>
     </div>
    `;
    popupdiv.classList.add('show');
    popupdiv.innerHTML = popup;
};

const popupclose = () => {
    let parent = document.querySelector('.popup');
    parent.innerHTML = null;
    parent.classList.remove('show');
};

const getSceleton = (name=scelet) => {
    return JSON.parse( localStorage.getItem(name) );
}

const removesect = (item) => {
    localStorage.removeItem(item);
    const menuObj = getSceleton();
    menuObj.navbar.pop(item);
    localStorage.setItem(menuObj.name, JSON.stringify(menuObj) );
};

const fetch = (num) => {
    let item = JSON.parse( localStorage.getItem(num) );
    let obj = new DomElement();
    Object.assign(obj, item);
    return obj;
};

const savechanges = (num) => {
    let form = document.getElementById('myForm');
    let item;
    if (localStorage.getItem(num)) {
        item = fetch(num);
        item.set(form[0].value, form[1].value, form[2].value, form[3].value, form[4].value);
    } else {
        item = new DomElement(form[0].value, form[1].value, form[2].value, form[3].value, form[4].value);
        const menuObj = getSceleton();
        menuObj.navbar.push(item.id);
        localStorage.setItem(menuObj.name, JSON.stringify(menuObj) );
    }
    item.store();
    
    popupclose();
};

const editMenu = (name) => {
    const menuObj = JSON.parse( localStorage.getItem(name) );
    const popupdiv = document.querySelector('.popup');
    let popup = `
    <div class="popup__body">
        <button type='submit' for="form" class="popup__save2" id="popup__add2" onclick='addsection()'>Add item</button>
        <button type='submit' for="form" class="popup__save" id="popup__add" onclick='changemenu()'>Save</button>
        <button type='submit' for="form" class="popup__close" id="popup__close" onclick='popupclose()'>Close</button>
        <form id="myForm" class="popup__container" id="popup__container">`;
    menuObj.navbar.forEach(item => {
            let sect = fetch(item);
            popup += `<label class="main-menu">
                        <span>Menu item ${sect.menuItem}</span>
                        <input type="text" name="${sect.id}" placeholder="New name">
                        <button id="popup__close" onclick='removesect(${sect.id})'>TO TRASH</button>
                        <button id="popup__close" onclick='moveup(${sect.id})'>Move Up</button>
                    </label>`;
    });
    popup += `</form></div>`; 
    popupdiv.classList.add('show');
    popupdiv.innerHTML = popup;
};

const changemenu = () => {
    const myForm = document.getElementById('myForm');
    const list = myForm.querySelectorAll('input');
    let i = 0;
    list.forEach(item => {
        let menuitem = fetch(item.name);
        if (item.value) {
            menuitem.menuItem = item.value;
            menuitem.store();
        }
        i += 1;
    });
};

const editThem = (name) => {
    const menuObj = JSON.parse( localStorage.getItem(name) );
    const popupdiv = document.querySelector('.popup');
    let popup = `
    <div class="popup__body">
        <button type='submit' for="form" class="popup__save2" id="popup__add" onclick='setchanges()'>Save Changes</button>
        <button type='submit' for="form" class="popup__close" id="popup__close" onclick='popupclose()'>Close</button>
        <form id="myForm" class="popup__container" id="popup__container">
            <label class="main-menu">
                    <span>Logo: </span>
                    <input type="text" name="logo">
            </label>
            <label class="main-menu">
                    <span>Background color: </span>
                    <input type="color" name="bg" value="${menuObj.style.bg}">
            </label>
            <label class="main-menu">
                    <span>Font color: </span>
                    <input type="color" name="color" value="${menuObj.style.color}">
            </label>
            <label class="main-menu">
                    <span>Title: </span>
                    <input type="text" name="title" placeholder="${menuObj.title}">
            </label>
            <label class="main-menu">
                    <span>Footer: </span>
                    <textarea name="foot">${menuObj.footer}</textarea>
            </label>
        </form>
    </div>`; 
    popupdiv.classList.add('show');
    popupdiv.innerHTML = popup;
};

const setchanges = () => {
    const form = document.getElementById('myForm');
    let item = getSceleton();
    if (item) {
        if (form.bg.value) item.style.bg = form.bg.value; 
        if (form.color.value) item.style.color = form.color.value;
        if (form.logo.value) item.brand = form.logo.value;
        if (form.title.value) item.title = form.title.value;
        if (form.foot.value) item.footer = form.foot.value;
    } 
    localStorage.setItem('scelet', JSON.stringify(item));
    
    popupclose();
};