/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
window.addEventListener('load', function() {
    const button = document.getElementById('chcolor');

    let color = JSON.parse( localStorage.getItem("color") );
    if (color !== null) {
        document.body.setAttribute('style', `background-color: ${color}`);
        // console.log(color);
    }

    button.addEventListener('click', function(e) {
        e.preventDefault();
        let color = setColor();
        document.body.setAttribute('style', `background-color: ${color}`)
        localStorage.setItem('color', color);
        // console.log( JSON.parse( localStorage.getItem("color") ) );
    })
});

function setColor() {
    var min = 0;
    var max = 255;
    var color = "#";
    for (var i = 0; i < 3; i++) {
      var dec = getRandomIntInclusive(min, max);
      color += dec.toString(16);
    }
    console.log(color)
    return color;
}
  
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}