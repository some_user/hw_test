/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/
window.addEventListener('load', function() {

    const forma = document.getElementById('forma');
    const login = document.getElementById('login');
    const password = document.getElementById('password');
    const submit = document.getElementById('submit');
    const exit = document.getElementById('exit');
    const greatings = document.getElementById('greatings')
    
    const submitEvent = (e) => {
        e.preventDefault();
        if (login.validity.valid && password.validity.valid) {
            let user = new Login(login.value, password.value);
            user.store();
            storeCurr(user);
            currUser = user;
            showForm(currUser);
        }
    }

    const exitEvent = (e) => {
        e.preventDefault();
        if (currUser !== -1) {
            localStorage.removeItem("currentUser");
            currUser = -1;
            showForm(currUser);
        }
    }

    submit.addEventListener('click', submitEvent);
    exit.addEventListener('click', exitEvent);



    let currUser = fetchCurr('currentUser');
    console.log(currUser);
    showForm(currUser);

});

class Login {
    constructor(name='user', password='password') {
        this.name = name;
        this.password = password;
    }
    stringify() {
        return JSON.stringify(this);
    }
    parse(usr) {
        return JSON.parse(usr);
    }
    store() {
        localStorage.setItem(`user_${this.name}`, this.stringify());
    }
    
    fetch(name) {
        const data = this.parse(localStorage.getItem(`user_${name}`));
        this.name = data.name;
        this.password =  data.password;
    }
    
}

const storeCurr = (obj) => {
    localStorage.setItem(`currentUser`, obj.name);
}

const fetchCurr = (name) => {
    const data = localStorage.getItem(name);
    if (data !== null) {
        let user = new Login();
        user.fetch(data);
        return user;
    } else {
        return -1;
    }
}

const showForm = (currUser) => {
    if (currUser === -1) {
        forma.classList.remove('invisible');
        exit.classList.add('invisible');
        greatings.innerText = "Fill that form!!!"
    } else {
        forma.classList.add('invisible');
        exit.classList.remove('invisible');
        greatings.innerText = `Hello, ${currUser.name}`
    }
}