
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : "23123", "age": 15, "password": "*****" }'


*/

function Obj() {};
Obj.prototype.setObj = function(name, age, password) {
  this.name = name;
  this.age = age;
  this.password = password;
};
let myObj = new Obj();

window.addEventListener('load', function() {
  const myForm2 = document.body.children.myForm2.children; 
  const myForm3 = document.body.children.myForm3.children;

  myForm2.agree.addEventListener('click', function(e) {
    e.preventDefault();
    myObj.setObj(myForm2.name.value, myForm2.age.value, myForm2.password.value);
    let jsonObj = JSON.stringify(myObj);
    console.log(myObj, jsonObj);
  });

  myForm3.convert.addEventListener('click', function(e) {
    e.preventDefault();
    let str = myForm3.string.value;
    let jsonObj = JSON.parse(str);
    console.log(str, jsonObj);
  });

});