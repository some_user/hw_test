/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
//   function FetchDemo(){
//   let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
//   
//           myHeaders.append("Content-Length", 255);
//           myHeaders.append("X-Custom-Header", "ProcessThisImmediately");
//   let options = {
//     method: 'POST', 
//     headers: headers,
//     body: body,
//     mode: 'nocors',
//   };
  
// };

  const url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";
  const url2 = "http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2"
  let myRequest = new Request(url);

  let myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");

  let options = {
    method: 'POST', 
    headers: myHeaders,
    mode: 'cors',
  };

  const resToJson = (res) => {
    if (res.ok) {
      return res.json();
    } else {
      throw new Error ('error in requests');
    }
  }

  const randomy = (num) => {
    let randnum =  Math.floor(Math.random() * (num - 1 + 1)) + 1;
    return (randnum > 0) ? randnum : (randnum * -1)
  }

  const randomPeopl = res => {
    return res[randomy(res.length)];
  }
  
  const render = (res) => {
    let person = res[0]._id;
    let friends = res[0].friends;
    console.log(res, person, friends);
    let html = `<h3> Person ID: ${person}</h3>
                <ul>
    `;
    friends.forEach(i => {
      html += `<li>${i.name}</li>`;
    });
    html += '</ul>';
    document.body.innerHTML = html;
  }

  fetch(myRequest, options)
    .then(resToJson)
    .then(randomPeopl)
    .then( res => {
       return fetch(url2, options)
        .then(resToJson)
    //     .then( friendsResponse)
    //     .then()
    })
    .then( render );