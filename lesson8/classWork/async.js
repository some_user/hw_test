/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
let obj = {};
async function getUserWithFriends(){
    const getUserResponse = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2")
    const users = await getUserResponse.json();

    // const selectedUserName = users[0];
    // console.log(selectedUserName);

    // const getUserFriends = await fetch("http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2");
    // const UserFriends = await getUserFriends.json();
  
    // console.log(UserFriends);
    // let { age, name, gender } = selectedUserName;
    // const CombinedUser = {
    //   age,
    //   name,
    //   gender,
    //   friends: UserFriends[0].friends
    // };
    // return CombinedUser;
    return users;
  }

    const table = (res) => {
        obj = res;
        let table = document.createElement('table');
        let i = 0;
        res.forEach((item) => {
            i++;
            let tr = document.createElement('tr');
            tr.innerHTML = `
                <td>${i}</td>
                <td>${item.company}</td> 
                <td>${item.balance}</td>
                <td data-data="${item.registered}" onclick="this.innerText = this.dataset.data">shou info</td> 
                <td><button name="address" data-data="${i-1}">shou info</button></td>
            `;

            table.appendChild(tr);
            let btn = tr.querySelector('button');
            btn.addEventListener('click', (e) => {
                e.preventDefault();
                btn.closest('td').innerHTML = (() => {
                    let addres = obj[btn.dataset.data];
                    let addrFullStr = '';
                    for (key in addres) {
                        addr += `key addres[key], `;
                    }
                })();
            })
        })
        document.body.appendChild(table);
    }

  var UserWithFriends = getUserWithFriends();
    UserWithFriends.then(table);
      // UserWithFriends.then( data => console.log('Final Person:', data));

        
        // result.appendChild(list());