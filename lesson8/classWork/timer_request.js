/*
172.17.13.181:3003/register/ -> POST
    {
        username: «»,
        email: «»
    }

172.17.13.181:3003/task/ -> POST
    {
        title: «»,
        description: «»,
        user: «»
    }

172.17.13.181:3003/users/ -> GET
172.17.13.181:3003/tasks/ -> GET

*/
window.addEventListener('load', function() {
	let getUsersUrl = "http://172.17.13.181:3003/register/";
	let xhr = new XMLHttpRequest();
	xhr.open('POST', getUsersUrl, true);
	xhr.setRequestHeader("Content-Type", "application/json");

	let ob = {
		name: '',
		email: ''
	}
	
	const form = document.getElementById('myForm1').children;
	form.request.addEventListener('click', function(e) {
		e.preventDefault()
		ob.name = form.text.value;
		ob.email = form.email.value;
		console.log(ob.name, ob.email);
		var y = JSON.stringify(ob);
		console.log(y);
		xhr.send(y);
	})
})