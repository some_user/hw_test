/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через
   element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/



function setBackground() {
  var myBackgroundTeg = document.getElementById('app');
  myBackgroundTeg.appendChild(child());
}

var child = function() {
  var div = document.createElement('div');
  var width = window.innerWidth;
  var height = window.innerHeight;
  var styles = `position: absolute; left: ${width / 4}px; top: ${height / 4}px; width: ${width / 2}px; height: ${height / 2}px; text-align: center;`;
  div.setAttribute('style', styles);
  var newColor = setColor()
  div.innerText = newColor;
  div.style.background = newColor;
  
  return div;
}

function setColor() {
  var min = 0;
  var max = 255;
  var color = "#";
  for (var i = 0; i < 3; i++) {
    var dec = getRandomIntInclusive(min, max);
    color += dec.toString(16);
  }
  console.log(color)
  return color;
}

function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

setBackground();