/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'white',
    changeColors: function(text){
      document.body.style.background = this.background;
      document.body.style.color = this.color;
      document.getElementById('header').innerText = text;
    }
  }

  let newcolors = {
    background: 'green',
    color: 'yellow',
  }

  let head = "I know how binding works in JS";

  // -- call method --
  // colors.changeColors.call( newcolors, head);

  // -- bind method --
  let bindFunc = colors.changeColors.bind(newcolors, head);
  let button = document.getElementById('press');
  button.addEventListener( 'click', bindFunc );

  //  -- apply method --
  // function changeColors(background, color){
  //   document.body.style.background = background;
  //   document.body.style.color = color;
  //   document.getElementById('header').innerText = this.head;
  // }

  // let text = {
  //   head: "I know how binding works in JS"
  // };

  // let cols = ['black', 'red'];
  // changeColors.apply(text, cols);
