/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
 
let Train = {};
Train.name = 'Night express';
Train.speedValue = 0;
Train.pasengers = 5;
Train.goin = function (speed) {
	let curr = this.speedValue;
	this.speedValue = speed || curr;
	console.log(`The train ${this.name} bare ${this.pasengers} per ${this.speedValue} km per hour.`);
	return `The train ${this.name} bare ${this.pasengers} per ${this.speedValue} km per hour.`;
}
Train.stop = function() {
	this.speedValue = 0;
	console.log(`The train ${this.name} had stoped. It's speed is ${this.speedValue} km per hour.`);
	return `The train ${this.name} had stoped. It's speed is ${this.speedValue} km per hour.`
}
Train.getPasengers = function(num) {
	this.pasengers += +num;
}

// Train.goin(60);
// Train.stop();
// Train.getPasengers(6);
// Train.goin(60);


document.addEventListener("DOMContentLoaded", function () {
	var speed = document.getElementById('speed1');
	speed.addEventListener("change", function () {
		shouInfo();
		document.getElementById('speed').innerText = Train.speedValue;
	})
	var pase = document.getElementById('pase');
	pase.addEventListener("change", function () {
		Train.getPasengers(+pase.value);
		shouInfo();
		document.getElementById('Pasengers').innerText = Train.pasengers;
		
	})
	function shouInfo() {
		document.getElementById('train-info').innerText = 
			(speed.value === 0) ? Train.stop() : Train.goin(+speed.value);
	}

});