/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/
  class Spell {
    constructor(name, spellFunc) {
      this.name = name;
      this.spell = spellFunc;
    }
    spell() {
      return spell();
    }
  }

  class SuperDude {
    constructor(name, power) {
      this.name = name;
      this.power = power
      this.whatPower(this.power);
    }

    whatPower(power) {
      power.forEach( item => {
        let obj = new Spell(item.name, item.spell);
        this.spell = obj.spell;
        let str = this.spell();
        this[item.name] = () => { console.log(str); };
        delete this.spell;
      })
    }
  }



  let superPowers = [
    { name:'Invisibility', spell: function(){ return `${this.name} hide from you`} },
    { name:'superSpeed', spell: function(){ return `${this.name} running from you`} },
    { name:'superSight', spell: function(){ return `${this.name} see you`} },
    { name:'superFroze', spell: function(){ return `${this.name} will froze you`} },
    { name:'superSkin',  spell: function(){ return `${this.name} skin is unbreakable`} },
  ];

  let Luther = new SuperDude('Luther', superPowers);
  
  Object.defineProperty(
    Luther,
    "name",
    {
      configurable: false,
      writable: false,
      enumerable: false
    });

  for (key in Luther) {
    Object.defineProperty(Luther, key, {
      gconfigurable: false,
      writable: false,
    });
  };
  
      // Тестирование: Методы должны работать и выводить сообщение.
      Luther.superSight();
      Luther.superSpeed();
      Luther.superFroze();
      Luther.Invisibility();
      Luther.superSkin();

