

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит 
        возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст 
        validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        
        Используя браузерное API для валидации форм реализовать валидацию 
        следующей формы:
        

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */

    const constraint = {
      'name': "Как тебя зовут дружище?!",
      'email': "Ну и зря, не получишь бандероль с яблоками!",
      'password': "Я никому не скажу наш секрет",
      'apples': "Ну хоть покушай немного... Яблочки вкусные",
      'masseg': "Фу, неблагодарный(-ая)!",
      'agree': "Необразованные живут дольше! Хорошо подумай!"
    };

    

    window.addEventListener("load", function () {
      const myForm = document.getElementById("myForm");
      const inputs = myForm.querySelectorAll('input');
      inputs.forEach(function(item) {
        item.setCustomValidity(constraint[item.id]);
      })

      const name = document.getElementById("name");
      name.addEventListener("change", function () {
        // console.log(name.validity);
        if( name.validity.tooShort  ){
            name.setCustomValidity(constraint[name.id]);
        } else {
            name.setCustomValidity('');
        }
        console.log( name.value );
      });

      const email = document.getElementById("email");
      email.addEventListener("change", function () {
        // console.log(email.validity);
        if( email.validity.valid  ){
            email.setCustomValidity(constraint[email.id]);
        } else {
            email.setCustomValidity('');
        }
        // console.log( email.value );
      });

      const password = document.getElementById("password");
      password.addEventListener("change", function () {  
        if( password.validity.tooShort  ){
            password.setCustomValidity(constraint[password.id]);
        } else {
            password.setCustomValidity('');
        }
      });

      const apples = document.getElementById("apples");
      apples.addEventListener("change", function () { 
        if( apples.value == 0){
            apples.setCustomValidity(constraint[apples.id]);
        } else {
            apples.setCustomValidity('');
        }
      });

      const masseg = document.getElementById("masseg");
      masseg.addEventListener("change", function () {  
        if( masseg.value !== 'Thanks'){
            masseg.setCustomValidity(constraint[masseg.id]);
        } else {
            masseg.setCustomValidity('');
        }
      });

      const agree = document.getElementById("agree");
      agree.addEventListener("change", function () {  
        console.log(agree.validity);
        if( agree.validity.valueMissing){
            agree.setCustomValidity(constraint[agree.id]);
        } else {
            agree.setCustomValidity('');
        }
      });

      const check = document.getElementById("check");
      const result = document.getElementById('result');
      check.addEventListener('click', function (e) {
        e.preventDefault();
        result.innerHTML = null;
        if (!myForm.reportValidity()) {
          alert('Incorrect input')
        } 
        if (!myForm.checkValidity()) {
          inputs.forEach( function(item) {
            if (!item.validity.valid) {
              result.innerHTML += `<span>ERROR on ${item.id}<span>`;
            } 
            else {
              result.innerHTML += 'OK '+ item.id + '<br/>';
            }
          });
        };
      });
    });